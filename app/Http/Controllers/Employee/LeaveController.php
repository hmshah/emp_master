<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\EmpLeaveBalanceManagement;
use App\Models\EmpLeaveMaster;
use App\Models\Employee;
use App\Models\LeaveMaster;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class LeaveController extends Controller
{
    public function index()
    {
        $leaveDetails = EmpLeaveMaster::with(['employee','leaveType'])
                                        ->where('emp_id',\Session::get('employee')->id)
                                        ->get();

        return view('employee.leave.index',compact('leaveDetails'));
    }

    public function create(Request $request)
    {
        $leaveTypes = LeaveMaster::whereStatus(LeaveMaster::ACTIVE)->get();

        $emp = Employee::with(['empAddDetails','empDepartment'])->whereId(\Session::get('employee')->id)->first();


        if ($request->isMethod('post')){

            if ($emp->id == $request->leave_assign_to || $emp->id == $request->leave_assign_to){
                return back()->with(['error'=>'Please your leave assign Properly...!!']);
            }

            $leaveDuration = explode(' - ',$request->Leave_duration);

            $totalDebitLeaveDays = CarbonPeriod::between($leaveDuration[0],$leaveDuration[1])->count();

            if($request->leave_apply_type == EmpLeaveMaster::HALF_DAY && $totalDebitLeaveDays == 1){
                $totalDebitLeaveDays = 0.5;
            }

            if ($totalDebitLeaveDays > 1 && $request->leave_apply_type == EmpLeaveMaster::HALF_DAY){
                return back()->with(['error' => 'you can not apply for half-day leave if you select more then 1 day ']);
            }

            $request->validate([
                'leave_type_id' => 'required',
                'leave_apply_type' => 'required',
                'Leave_duration' => 'required',
                'leave_reason' => 'required',
            ],[
                'leave_type_id.required' => 'leave type is Required',
                'leave_apply_type.required' => 'leave mode is Required',
                'Leave_duration.required' => 'Leave_duration is Required',
                'leave_reason.required' => 'leave_reason is Required'
            ]);

            EmpLeaveMaster::create([
                'emp_id' => $emp->id,
                'dept_id' => $emp->empDepartment->id,
                'leave_type_id' => $request->leave_type_id,
                'start_date' => Carbon::parse($leaveDuration[0])->format('Y-m-d h:m:s'),
                'end_date' => Carbon::parse($leaveDuration[1])->format('Y-m-d h:m:s'),
                'total_leave' => $totalDebitLeaveDays,
                'leave_assign_id' => $request->leave_assign_to,
                'emp_leave_of_month' => Carbon::now()->format('Y-m'),
                'leave_apply_type' => $request->leave_apply_type,
                'leave_status' => EmpLeaveMaster::HOLD,
                'status' => EmpLeaveMaster::PENDING,
                'reason' => $request->leave_reason
            ]);

            return redirect()->route('emp-leave-view')->with(['success' => 'Your Application For Leave Request successfully created..!!']);
        }
        return view('employee.leave.sendRequest',compact('leaveTypes','emp'));
    }

    public function balance()
    {
        $allLeaves = LeaveMaster::whereStatus(LeaveMaster::ACTIVE)->get();

        $empTakenLeave = [];
        $existLeave = [];
        foreach($allLeaves as $leave){
            $empTakenLeave[$leave->leave_type] = EmpLeaveMaster::getLeaveDataById($leave->id);
            $existLeave[$leave->leave_type] = $leave->total_leave - $empTakenLeave[$leave->leave_type];
        }

        return view('employee.leave.balance',compact('empTakenLeave','allLeaves','existLeave'));
    }

    public function requestLeaveView()
    {
        $employeeData = Employee::whereId(\Session::get('employee')->id)->first();

        $getRequestDetails = EmpLeaveMaster::whereNull('approved_at')->where('status',EmpLeaveMaster::PENDING)
                                          ->where('leave_status',EmpLeaveMaster::HOLD)
                                          ->where('dept_id',$employeeData->department_id)->get();

        return view('employee.leave.leaveRequestView',compact('employeeData','getRequestDetails'));
    }

    public function leaveRequestEdit(Request $request, $id)
    {
        $getRequestDetails = EmpLeaveMaster::with('employee','leaveType')->where('id',$id)->first();
        return view('employee.leave.editRequest',compact('getRequestDetails'));
    }

    public function UpdateLeaveStatus(Request $request, $id)
    {
        $leaveDetails = EmpLeaveMaster::whereStatus(EmpLeaveMaster::PENDING)->where('leave_status',EmpLeaveMaster::HOLD)->first();
        if ($request->isMethod('post')){

            if ($request->leave_status == 1){
                    return back()->with(['error' => 'Please Select Approved/Reject Status...!!']);
            }

            if ($request->leave_status == EmpLeaveMaster::APPROVED){
                $leaveDetails->update([
                   'status' =>  EmpLeaveMaster::APPROVED,
                   'leave_status' =>  EmpLeaveMaster::DEBIT,
                    'approved_at' => Carbon::now()
                ]);
            }

            if ($request->leave_status == EmpLeaveMaster::REJECT){
                $leaveDetails->update([
                    'status' =>  EmpLeaveMaster::REJECT,
                    'leave_status' =>  EmpLeaveMaster::CREDIT,
                    'approved_at' => Carbon::now()
                ]);
            }

            return redirect()->route('emp-dashboard')->with(['success' => 'Your Records Successfully Updated..!!']);
        }
    }
}
