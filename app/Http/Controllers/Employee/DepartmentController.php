<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Employee;
use Illuminate\Http\Request;
use function Symfony\Component\HttpFoundation\Session\Storage\Handler\commit;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $departments = Department::with(['teamLeadId','deptMgrId'])->get();
        return view('employee.department.index',compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departmentEmp = Employee::with(['empDepartment'])->where('emp_status',Employee::ACTIVE)->get();
        return view('employee.department.create',compact('departmentEmp'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $DeptNo = $empId = rand('100', '999');
        $Department =  Department::create([
                            'dept_no' => 'DEPT'.$DeptNo,
                            'dept_name' => $request->department_name,
                            'dept_leader_id' => $request->select_tl_id,
                            'dept_manager_id' => $request->select_mgr_id,
                            'status' => Department::ACTIVE
                        ]);

        $employeeIds = [
            $selectTLId  = $request->select_tl_id,
            $selectMgrId = $request->select_mgr_id
        ];

        Employee::whereIn('id',$employeeIds)
                                ->get()->map(function ($emp) use($Department,$request){
                                        $emp->update([
                                            'department_id' => $Department->id,
                                         ]);
                                });

        return redirect()->route('emp-department-view')->with(['success' => 'Your Record Created Successfully..!!!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::with(['teamLeadId','deptMgrId'])->whereId($id)->where('status','=',1)->first();
        $departmentEmp = Employee::whereDepartmentId($department->id)->pluck('name','id');

        if (!isset($departmentEmp))
                return redirect()->back()->with(['error' => 'Not exist Records from your Selected Department...!!']);

        return view('employee.department.update',compact('department','departmentEmp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('put')){
            $department = Department::with(['teamLeadId','deptMgrId'])->whereId($id)->first();

            $department->update([
                'dept_manager_id' => $request->select_mgr_id,
                'dept_leader_id' => $request->select_tl_id
            ]);
            return redirect()->route('emp-department-view')->with(['success' => 'Your Records Update Successfully...!!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
