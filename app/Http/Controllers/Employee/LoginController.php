<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Library\helper;

class LoginController extends Controller
{
    public function index(Request $request)
    {
          if ($request->isMethod('post')){
              $this->validate($request, [
                  'employee_id' => 'required',
                  'password' => 'required'

              ], [
                  'employee_id.required' => 'Employee Id is Required',
                  'password.required' => 'Password is Required'
              ]);

              $employee = Employee::where('emp_id',$request->employee_id)->Where('password', $request->password)->first();

              if (!$employee)
                  return redirect()->back()->with(['error' => 'Invalid Username or Tracking ID']);

              if($request->password != $employee->password)
                  return redirect()->back()->with(['error'=> 'Password is Not Match with Username!']);

              \Session::put('employee', $employee);

              Employee::where('id',$employee->id)->update([
                  'last_logged_in_ip' => Helper::getClientIp(),
                  'last_logged_in_at' => Carbon::now()
              ]);

              return redirect()->route('emp-dashboard')->with([
                  'success' => 'Welcome to'. $employee->name .' account'
              ]);
          }
        return view('employee.authentication.login');
    }

    public function forgetPassword()
    {
        return view('employee.authentication.forget-password');
    }

    public function logout()
    {
        \Session::forget('employee');
        return redirect()->route('emp-login');
    }
}
