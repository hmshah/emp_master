<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\SalaryDeduction;
use App\Models\SalaryMaster;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Knp\Snappy\Pdf;

class SalaryController extends Controller
{
    public function index(Request $request)
    {
        $salaryDetails = SalaryMaster::with(['emp','empAddress'])->whereEmpId(\Session::get('employee')->id)->get();

        $salaryDetail = $salaryDetails->filter(function($val,$key) use($request){
            $sal = SalaryMaster::getSalaryData($val,$request->select_month);
            return $sal;
        })->first();

        if (!$salaryDetail)
             return back()->with('error','Records is Not Found of Selected Month..!!');

        $states = State::whereStatus(State::ACTIVE)->get();
        $deduction = SalaryDeduction::with('salMaster')->where('sal_master_id',$salaryDetail->id)->first();

        $totalDeduction = $deduction->professional_tax + $deduction->Leave_deduction +$deduction->pf_deduction + $deduction->mediclaim_deduction;

        return view('employee.Salary.payslip',compact('salaryDetail','states','deduction','totalDeduction'));
    }

    public function download(Request $request)
    {
        $employee = Employee::whereId(\Session::get('employee')->id)->first();

        $salaryDetails = SalaryMaster::with(['emp','empAddress'])->whereEmpId(\Session::get('employee')->id)->get();

        $salaryDetail = $salaryDetails->filter(function($val,$key) use($request){
            $sal = SalaryMaster::getSalaryData($val,$request->month);
            return $sal;
        })->first();

        $deduction = SalaryDeduction::with('salMaster')->where('sal_master_id',$salaryDetail->id)->first();

        $totalDeduction = $deduction->professional_tax + $deduction->Leave_deduction +$deduction->pf_deduction + $deduction->mediclaim_deduction;

        $filename = 'Salary_slip_'. $employee->emp_id;
        if ($request->download){
            $snappy = new Pdf(base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));
            $snappy->generateFromHtml(
                view('employee.Salary.download', [
                    'filename' => $filename,
                    'employee' => $employee,
                    'salaryDetail' => $salaryDetail,
                    'deduction' => $deduction,
                    'totalDeduction' => $totalDeduction,
                ])->__toString(), storage_path("app/".$filename.".pdf"), [
                'orientation' => 'Portrait',
                'page-height' => 297,
                'page-width'  => 210,
            ], true);
            return response()->download(storage_path("app/".$filename.".pdf"))->deleteFileAfterSend(true);
        }

    }

    public function salaryGenerate(Request $request)
    {
        if ($request->isMethod('post')){

            SalaryMaster::salaryCalculation();
            return back()->with(['success' => 'Your PaySlip generate Successfully']);
        }
    }
}
