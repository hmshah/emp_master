<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\AttendanceLog;
use Illuminate\Http\Request;
use App\Library\helper;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class AttendanceController extends Controller
{

    public function dailySwipe(Request $request)
    {
        if ($request->isMethod('post')){
            $ip = $request->ip();

            $empSwipe = AttendanceLog::create([
               'emp_id' => \Session::get('employee')->id,
               'ip_address' => $ip,
               'log_token' => strtoupper(Str::random(40)),
               'log_time' => now(),
               'type' => $request->type,
               'status' => AttendanceLog::ACTIVE,
            ]);

            if (!empty(\Session::get('emp_swipe'))){
                Session::forget('emp_swipe');
            }

            Session::push('emp_swipe',$empSwipe);

            return redirect()->back()->with(['success' => 'Your Action done Successfully ..!!']);
        }

    }

    public function create()
    {
        //
    }

    public function attendanceApprove()
    {

    }
}
