<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmpTaskMaster;
use App\Models\TaskAssignMgt;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $getEmpTasks = EmpTaskMaster::where('emp_id',\Session::get('employee')->id)->where('dept_id',\Session::get('employee')->department_id)->get();
        return view('employee.task.index',compact('getEmpTasks'));
    }

    public function taskHistory()
    {
        $getEmpTasks = TaskAssignMgt::with(['empTaskMaster','employee'])->where('emp_id',\Session::get('employee')->id)->get();
        return view('employee.task.history',compact('getEmpTasks'));
    }

    public function create(Request $request)
    {
       if ($request->isMethod('post')){

            $this->validate($request, [
                'task_subject' => 'required',
                'task_current_status' => 'required',
                'select_dept_id' => 'required',
                'emp_id' => 'required',
                'task_duration' => 'required',
                'status' => 'required',
                'description' => 'required'
            ],[
                'task_subject.required' => 'Task Subject required',
                'task_current_status.required' => 'Task Current Status required',
                'select_dept_id.required' => 'Department is required',
                'emp_id.required' => 'Employee is required',
                'task_duration.required' => 'Task Duration required',
                'status.required' => 'status required',
                'description.required' => 'description test'
            ]);

            $dateArr = explode(' - ',$request->task_duration);

            $start_at = $dateArr[0];
            $end_at = $dateArr[1];

              $task =  EmpTaskMaster::create([
                   'emp_id' => $request->emp_id,
                   'dept_id' => $request->select_dept_id,
                   'created_by' => \Session::get('employee')->id,
                   'start_date' => Carbon::parse($start_at)->format('Y-m-d h:m:s'),
                   'end_date' => Carbon::parse($end_at)->format('Y-m-d h:m:s'),
                   'description' => $request->description,
                   'subject' => $request->task_subject,
                   'status' => $request->status,
                   'task_status' => $request->task_current_status
               ]);

               TaskAssignMgt::create([
                   'task_master_id' => $task->id,
                   'emp_id' => $request->emp_id,
                   'status' => $task->status
               ]);

           return redirect()->route('emp-task-lists')->with(['success' => 'Task Create Successfully..!!']);
       }
        return view('employee.task.create');
    }

    public function getEmployeeDptWise(Request $request)
    {
        $departmentEmp = Employee::where('department_id',$request->deptId)->pluck('name','id');

        if (!isset($departmentEmp))
            return redirect()->back()->with(['error' => 'Not exist Records from your Selected Department...!!']);

        return response()->json($departmentEmp,'200');
    }

    public function getEmployeeDptWisee(Request $request)
    {
        $departmentEmp = Employee::where('department_id',$request->deptId)->pluck('name','id');

        if (!isset($departmentEmp))
            return redirect()->back()->with(['error' => 'Not exist Records from your Selected Department...!!']);

        return response()->json($departmentEmp,'200');
    }

    public function update(Request $request)
    {
        $taskDetail = EmpTaskMaster::where('id',$request->id)->first();

        if ($request->isMethod('post')){
            $dateArr = explode(' - ',$request->task_duration);

            $start_at = $dateArr[0];
            $end_at = $dateArr[1];

         $taskDetail->update([
                'emp_id' => $request->emp_id,
                'dept_id' => $request->select_dept_id,
                'created_by' => \Session::get('employee')->id,
                'start_date' => Carbon::parse($start_at)->format('Y-m-d h:m:s'),
                'end_date' => Carbon::parse($end_at)->format('Y-m-d h:m:s'),
                'description' => $request->description,
                'subject' => $request->task_subject,
                'status' => $request->status,
                'task_status' => $request->task_current_status
            ]);

         $existVal = TaskAssignMgt::whereEmpId($request->emp_id)->where('task_master_id',$taskDetail->id);
          if($existVal->exists()){
              $existVal->update([
                  'task_master_id' => $taskDetail->id,
                  'emp_id' => $request->emp_id,
                  'status' => $request->status
              ]);
          }else{
              TaskAssignMgt::create([
                  'task_master_id' => $taskDetail->id,
                  'emp_id' => $request->emp_id,
                  'status' => $request->status
              ]);
          }
            return redirect()->route('emp-task-lists')->with(['error' => 'Your Records Updated successfully..!!']);
        }

        return view('employee.task.edit',compact('taskDetail'));
    }

    public function testForm()
    {
        return view('employee.testForm');
    }
}
