<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Department;
use App\Models\EmpAddress;
use App\Models\EmpEducation;
use App\Models\Employee;
use App\Models\SalaryMaster;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $departments = Department::pluck('dept_name','id');
        if ($request->isMethod('post'))
        {
            $employee = \DB::transaction(function () use ($request) {
                $request->validate([
                    'title' => 'required',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'mobile' => 'required',
                    'birth_date' => 'required',
                    'designation' => 'required',
                    'email_id' => 'required',
                    'password' => 'required',
                    'salary' => 'required',
                    'nationality' => 'required',
                    'emp_type' => 'required',
                    'joining_date' => 'required',
                    'address' => 'required',
                    'city' => 'required',
                    'state_id' => 'required',
                    'district' => 'required',
                    'pincode' => 'required'
                ],[
                    'title.required' => 'title is required',
                    'first_name.required' => 'First Name is required',
                    'last_name.required' => 'Last Name is required',
                    'mobile.required' => 'Mobile is required',
                    'birth_date.required' => 'Birthday is required',
                    'designation.required' => 'designation is required',
                    'email_id.required' => 'email Id is required',
                    'password.required' => 'password is required',
                    'salary.required' => 'salary is required',
                    'nationality.required' => 'nationality is required',
                    'emp_type.required' => 'employee type is required',
                    'joining_date.required' => 'joining date is required',
                    'address.required' => 'address is required',
                    'city.required' => 'city is required',
                    'state_id.required' => 'State is required',
                    'district.required' => 'District is required',
                    'pincode.required' => 'District is required',
                ]);

//                dd($request->all());
                //random generate employee id
                $empId = rand('1000', '9999');

                $employee = Employee::create([
                    'emp_id' => 'E-'.$empId,
                    'title' => $request->title,
                    'name' => $request->first_name.' '.$request->last_name,
                    'password' => $request->password,
                    'designation' => $request->designation,
                    'email_id' => $request->email_id,
                    'department_id' => $request->department_id,
                    'salary' => $request->salary,
                    'address' => $request->address,
                    'gender' => $request->gender,
                    'contact_no' => $request->mobile,
                    'nationality' => $request->nationality,
                    'birthday' => Carbon::parse($request->birth_date)->format('Y-m-d'),
                    'joining_date' => Carbon::parse($request->joining_date)->format('Y-m-d h:i:s'),
                    'emp_type' => $request->emp_type,
                    'emp_access_mode' => $request->employee_access,
                    'marital_status' => $request->marital_status,
                    'emp_position' => $request->employee_position
                ]);

                 EmpAddress::create([
                    'employee_id' => $employee->id,
                    'city' => $request->city,
                    'state_id' => $request->state_id,
                    'district' => $request->district,
                    'pin_code' => $request->pincode,
                    'address' => $request->address,
                    'country_id' => $request->nationality
                ]);

                EmpEducation::create([
                    'emp_id' => $employee->id
                ]);

                return $employee;
            });

            \Session::put('registered_emp', $employee);

            return redirect()->route('emp-register-thanks')->with([
                'success' => 'Your Registration has been successfully completed!',
            ]);

        }
        $countries = Country::where('status',1)->get();
        return view('employee.authentication.register.details',compact('countries','departments'));
    }

    public function thanks()
    {
        if (!\Session::has('registered_emp')) {
            return redirect()->route('emp-register');
        }

        $employee = Employee::where('id',\Session::get('registered_emp')->id)->first();

        \Session::forget('registered_emp');

        return view('employee.authentication.register.thanks', [
            'employee' => $employee
        ]);
    }

    public function getStates(Request $request)
    {
        $states = State::where('country_id',$request->country_id)->get();
        return response()->json($states,200);
    }
}
