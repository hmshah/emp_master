<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Middleware\Employee\Auth;
use App\Models\AttendanceLog;
use App\Models\EmpLeaveMaster;
use App\Models\Employee;
use App\Models\SalaryMaster;
use App\Models\State;
use App\Models\UpcomingHoliday;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $upcomingHolidays = UpcomingHoliday::where('status',1)->get();

        $totalSalarySlip = SalaryMaster::whereEmpId(\Session::get('employee')->id)->get()->count();

        $employeeData = Employee::whereId(\Session::get('employee')->id)->first();

        if ($employeeData->emp_position != 5){
            $getRequestData = EmpLeaveMaster::with('department')->whereNull('approved_at')
                ->where('status',EmpLeaveMaster::PENDING)
                ->where('leave_status',EmpLeaveMaster::HOLD)
                ->where('dept_id',$employeeData->department_id)
                ->where('leave_assign_id',$employeeData->id)->get();
        }else{
            $getRequestData = EmpLeaveMaster::with('department')->whereNull('approved_at')
                ->where('status',EmpLeaveMaster::PENDING)
                ->where('leave_status',EmpLeaveMaster::HOLD)
                ->where('leave_assign_id',$employeeData->id)->get();
        }

//        $departmentTlIds = [];
//        $departmentPmIds = [];
//
//        foreach ($getRequestData as $val)
//        {
//            $departmentTlIds[] = $val->department->dept_leader_id;
//            $departmentPmIds[] = $val->department->dept_manager_id;
//        }
//        $allHeadOfDepartmentIds = array_merge(array_unique($departmentTlIds),array_unique( $departmentPmIds));
        $totalReqData = $getRequestData->count();

        $getEmpSwipeData = AttendanceLog::where('emp_id',\Session::get('employee')->id)->whereDate('log_time', Carbon::today())->where('status',AttendanceLog::ACTIVE)->get();

        return view('employee.dashboard',compact('upcomingHolidays','employeeData','totalSalarySlip','getRequestData','totalReqData','getEmpSwipeData'));
    }
}
