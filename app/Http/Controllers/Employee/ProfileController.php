<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\EmpAddress;
use App\Models\EmpEducation;
use App\Models\Employee;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function Symfony\Component\HttpFoundation\Session\Storage\Proxy\validateId;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $emp = Employee::with('empAddDetails')->whereId(\Session::get('employee')->id)->first();
        $states = State::whereStatus(State::ACTIVE)->get();
        $departments = Department::pluck('dept_name','id');

        if ($request->isMethod('post')){
            $request->validate([
                'birth_date' =>'required',
                'contact_no' =>'required',
                'marital_status' =>'required',
                'gender' =>'required',
                'email_id' =>'required',
                'address' =>'required',
                'landmark' =>'required',
                'city' =>'required',
                'pin_code' =>'required',
                'duration_of_graduation' =>'required',
                'state_id' =>'required',
            ],[
                'birth_date.required' => 'Birth date is required',
                'contact_no.required' => 'Contact no is required',
                'marital_status.required' => 'marital status is required',
                'gender.required' => 'Gender is required',
                'email_id.required' => 'Email Id is required',
                'address.required' => 'Address is required',
                'landmark.required' => 'Land Mark is Required',
                'city.required' => 'City Is Required',
                'pin_code.required' => 'Pin Code is required',
                'duration_of_graduation.required' => 'Time Duration of Graduation is required',
                'state_id.required' => 'State id is required',
            ]);

            $emp->update([
                'birthday' => Carbon::parse($request->birth_date)->format('Y-m-d'),
                'contact_no' => $request->mobile,
                'marital_status' => $request->marital_status,
                'gender' => $request->gender,
                'email_id' => $request->email_id,
                'address' => $request->address
            ]);

            $employeeAddress = EmpAddress::whereEmployeeId($emp->id)->first();

            $employeeAddress->update([
                'city' => $request->city,
                'pin_code' => $request->pincode,
                'state_id' => $request->state_id,
                'district' => $request->district,
                'address' => $request->address
            ]);

            $empEducationdata = EmpEducation::where($emp->id)->first();
            EmpEducation::create([
                'graduation' => $request->graduation,
                'institute_name' => $request->institute_name,
                'time_duration' => $request->duration_of_graduation,
                'state_id' => $request->grad_state_id,
            ]);

        }
        return view('employee.profile',compact('emp','states','departments'));
    }
}
