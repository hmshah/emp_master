<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AttendanceLog
 *
 * @property-read \App\Models\Employee|null $employee
 * @method static \Illuminate\Database\Eloquent\Builder|AttendanceLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AttendanceLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AttendanceLog query()
 * @mixin \Eloquent
 */
class AttendanceLog extends Model
{
    use HasFactory;

    const ACTIVE = 1, INACTIVE = 2;
    const LOGIN = 1, LOGOUT = 2;

    protected $fillable = [
        'emp_id','ip_address','log_token','log_time','type','status'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class,'emp_id');
    }

    public static function swipeTypes($type = null)
    {
        $swipeTypes = [
          1 => 'Login',
          2 => 'Logout'
        ];

        return isset($type) ? $swipeTypes[$type] : '';
    }
}
