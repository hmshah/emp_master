<?php

namespace App\Models;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SalaryMaster
 *
 * @property int $id
 * @property string $emp_id
 * @property string $total
 * @property string $basic_salary
 * @property string $hra
 * @property string $conveyance
 * @property string $medical_allowance
 * @property string $other_allowance
 * @property string $advance_statutory_bonus
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Employee|null $emp
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster query()
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereAdvanceStatutoryBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereBasicSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereConveyance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereEmpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereHra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereMedicalAllowance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereOtherAllowance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $generated_month
 * @property-read \App\Models\EmpAddress|null $empAddress
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryMaster whereGeneratedMonth($value)
 */
class SalaryMaster extends Model
{
    use HasFactory;
    protected $fillable = [
        'emp_id','total','basic_salary','hra','conveyance','medical_allowance','other_allowance','generated_month','advance_statutory_bonus'
    ];

    // relation with employee
    public function emp()
    {
        return $this->hasOne(Employee::class,'id','emp_id');
    }

    public function empAddress()
    {
        return $this->hasOne(EmpAddress::class,'employee_id','emp_id');
    }

    public static function getSalaryData($userSalDetail,$reqVal)
    {
        $salaryData = null;

        if ($userSalDetail->generated_month ==  Carbon::parse($reqVal)->format('Y-m'))
        {
            $salaryData = self::first();
        }
        return $salaryData;
    }

    public static function salaryCalculation()
    {
        $employees = Employee::where('emp_status','=',Employee::ACTIVE)->get();

        $employees->map(function ($employee) {
            $totalSalary = $employee->salary;
            $basicSal = $totalSalary*0.50;
            $HRA = $totalSalary*0.20;
            $conveyance = $totalSalary*4.5/100;
            $MedicalAllowance = $totalSalary*3.5/100;
            $otherAllowance = $totalSalary*0.18;
            $advanceStatutaryBonus = $totalSalary*0.04;
            $pfDeduction = $totalSalary * 0.052;
            $profTax = 200;
            $mediclaimDeduction = 451;

            $getSalary = SalaryMaster::where('emp_id',$employee->id)->get();

            //current Month check
            $currentMonth = Carbon::now()->format('M-Y');

            //joining to current all month array list set
            $result = CarbonPeriod::create($employee->joining_date, '1 month',$currentMonth);

            $allMonthList = [];
            foreach ($result as $dt) {
                $allMonthList[] = $dt->format("Y-m");
            }

            // get all exist salary's Month
            $getEmpSalMonthLists = $getSalary->map(function($sal){
                                        return $sal->generated_month;
                                    });

            //getting data convert in array format
            $empSalMonListArr = $getEmpSalMonthLists->toArray();

            //getting total Leave Month wise
            $totalLeaveDeduction = [];
            foreach ($allMonthList as $monthList){
                $totalLeaveParticularMonth = EmpLeaveMaster::whereEmpId($employee->id)->where('emp_leave_of_month',$monthList)
                                                             ->whereStatus(EmpLeaveMaster::APPROVED)->sum('total_leave');
                $perDay = $totalSalary/30;
                $totalLeaveDeductAmount = $perDay * $totalLeaveParticularMonth;
                $totalLeaveDeduction[$monthList] = $totalLeaveDeductAmount;
            }

            //getting the difference of between of 2 array ** check month for generate Payslip
            $MonthDiffArr = array_diff($allMonthList, $empSalMonListArr);

            if (empty($MonthDiffArr)){
                return redirect()->route('emp-dashboard')->with(['error' => 'All Payslip Is Generated Successfully And Nothing to remain to generate Payslip of any employee..!!']);
            }

            foreach($MonthDiffArr as $Monthdiff){
                $empSal = self::create([
                    'emp_id' => $employee->id,
                    'total' => $totalSalary,
                    'basic_salary' => $basicSal,
                    'hra' => $HRA,
                    'conveyance' => $conveyance,
                    'medical_allowance' => $MedicalAllowance,
                    'other_allowance' => $otherAllowance,
                    'advance_statutory_bonus' => $advanceStatutaryBonus,
                    'generated_month' => $Monthdiff
                ]);

                SalaryDeduction::create([
                    'sal_master_id' => $empSal->id,
                    'professional_tax' => $profTax,
                    'pf_deduction' => $pfDeduction,
                    'Leave_deduction' => array_key_exists($Monthdiff,$totalLeaveDeduction) ? $totalLeaveDeduction[$Monthdiff] : 0,
                    'mediclaim_deduction' => $mediclaimDeduction
                ]);
            }
        });
    }
}
