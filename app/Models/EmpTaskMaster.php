<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmpTaskMaster
 *
 * @property-read \App\Models\Department|null $department
 * @property-read \App\Models\Employee|null $employee
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $emp_id
 * @property int $dept_id
 * @property int $created_by
 * @property string $start_date
 * @property string $end_date
 * @property string $description
 * @property string $subject
 * @property int $status 1:active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereDeptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereEmpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpTaskMaster whereUpdatedAt($value)
 */
class EmpTaskMaster extends Model
{
    use HasFactory;

    const HOLD = 1, IN_PROCESS = 2, TRICKY = 3, COMPLETED = 4;

    protected $fillable = [
        'emp_id','dept_id','created_by','start_date','end_date','description','subject','status','task_status'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class,'emp_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class,'dept_id');
    }

    public static function currentTaskStatus($id = null)
    {
        $status = [
            1 => 'hold',
            2 => 'In-Progress',
            3 => 'Tricky',
            4 => 'Completed'
        ];

        if(isset($id)){
            return $status[$id];
        }else{
            return $status;
        }
    }
}
