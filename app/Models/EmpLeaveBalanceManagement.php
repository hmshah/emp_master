<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmpLeaveBalanceManagement
 *
 * @property int $id
 * @property int $leave_master_id
 * @property int $total_leave
 * @property int $status 1:Active,2:InActive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\LeaveMaster|null $leaveMaster
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement whereLeaveMasterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement whereTotalLeave($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $emp_leave_master_id
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveBalanceManagement whereEmpLeaveMasterId($value)
 */
class EmpLeaveBalanceManagement extends Model
{
    use HasFactory;

    const ACTIVE = 1, INACTIVE = 2;
    protected $fillable = [
       'emp_leave_master_id','leave_master_id','total_leave','status'
    ];

    public function leaveMaster()
    {
        return $this->belongsTo(EmpLeaveMaster::class,'emp_leave_master_id');
    }
}
