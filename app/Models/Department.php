<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Department
 *
 * @property int $id
 * @property string $dept_no
 * @property string $dept_name
 * @property int $dept_manager_id
 * @property int $dept_leader_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Employee|null $deptMgrId
 * @property-read \App\Models\Employee|null $teamLeadId
 * @method static \Illuminate\Database\Eloquent\Builder|Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereDeptLeaderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereDeptManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereDeptName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereDeptNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $status 1:Active,2:In-Active
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereStatus($value)
 */
class Department extends Model
{
    use HasFactory;

    const ACTIVE = 1, INACTIVE = 2;
    protected $fillable = [
      'dept_no','dept_name','dept_manager_id','dept_leader_id','status'
    ];

    // relation for get TL data
    public function teamLeadId()
    {
        return $this->belongsTo(Employee::class,'dept_leader_id');
    }

    // relation for get PM data
    public function deptMgrId()
    {
        return $this->belongsTo(Employee::class,'dept_manager_id');
    }

    public static function LeadNameByDept($deptId)
    {
        $TeamLeadDetail = self::with('teamLeadId','deptMgrId')->where('id',$deptId)->first();

        $leadIds = [
            $TeamLeadDetail->teamLeadId->id,
            $TeamLeadDetail->deptMgrId->id
        ];

        $leadEmployeeData = Employee::whereIn('id',$leadIds)->get();
        return $leadEmployeeData;
    }

    /*Employee department name get */
    public static function empDepartment($deptId)
    {
      $deptName = self::whereId($deptId)->pluck('dept_name')->first();
      return $deptName;
    }

    public static function departments($id = null)
    {
        $department = self::whereStatus(self::ACTIVE)->pluck('dept_name', 'id');

        if (isset($id)){
           return $department[$id];
        }else{
           return $department;
        }
    }
}
