<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmpLeaveMaster
 *
 * @property int $id
 * @property int $emp_id
 * @property int $dept_id
 * @property int $leave_type_id
 * @property string $start_date
 * @property string $end_date
 * @property int $leave_apply_type 1:Full_day,2:Half_day
 * @property int $status 1:Pending,2:Approve,3:Reject
 * @property string|null $image
 * @property string $reason
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Employee|null $employee
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereDeptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereEmpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereLeaveApplyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereLeaveTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $total_leave
 * @property int $leave_status 1:Credit,2:Debit,3:Hold
 * @property string $emp_leave_of_month
 * @property string|null $approved_at
 * @property-read \App\Models\LeaveMaster|null $leaveType
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereApprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereEmpLeaveOfMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereLeaveStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereTotalLeave($value)
 * @property int $leave_assign_id
 * @property-read \App\Models\Department|null $department
 * @method static \Illuminate\Database\Eloquent\Builder|EmpLeaveMaster whereLeaveAssignId($value)
 */
class EmpLeaveMaster extends Model
{
    use HasFactory;

    const CREDIT = 1, DEBIT = 2, HOLD = 3;
    const PENDING = 1, APPROVED = 2, REJECT = 3;
    const FULL_DAY = 1, HALF_DAY = 2;

    protected $fillable = [
       'emp_id','dept_id','leave_type_id','start_date','end_date','total_leave','leave_assign_id','leave_apply_type','leave_status','approved_at','emp_leave_of_month','status','image','reason'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class,'emp_id','id');
    }

    public function leaveType()
    {
        return $this->belongsTo(LeaveMaster::class,'leave_type_id','id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class,'dept_id');
    }

    public static function leaveApplyModes($id)
    {
        $applyType = null;
        $leaveApplyTypes = [
            1 => 'Full Day',
            2 => 'Half Day'
        ];
        return $applyType = $leaveApplyTypes[$id];
    }

    /**
    * Leave check of user and calculation of applying leave only approval
     */
    public static function getLeaveDataById($id)
    {
        $getLeaveData = self::whereLeaveTypeId($id)->whereEmpId(\Session::get('employee')->id)->whereLeaveStatus(EmpLeaveMaster::DEBIT)->whereStatus(EmpLeaveMaster::APPROVED)->whereNotNull('approved_at')->sum('total_leave');
        return $getLeaveData;
    }
}
