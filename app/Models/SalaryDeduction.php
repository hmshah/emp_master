<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SalaryDeduction
 *
 * @property int $id
 * @property int $sal_master_id
 * @property float $professional_tax
 * @property float $pf_deduction
 * @property float $Leave_deduction
 * @property float $mediclaim_deduction
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\SalaryMaster|null $salMaster
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction query()
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction whereLeaveDeduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction whereMediclaimDeduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction wherePfDeduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction whereProfessionalTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction whereSalMasterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SalaryDeduction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SalaryDeduction extends Model
{
    use HasFactory;

    protected $fillable = [
        'sal_master_id','professional_tax','pf_deduction','Leave_deduction','mediclaim_deduction'
    ];

    public function salMaster()
    {
        return $this->belongsTo(SalaryMaster::class,'sal_master_id');
    }
}
