<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UpcomingHoliday
 *
 * @property int $id
 * @property string $date
 * @property string $name
 * @property string $year
 * @property int $status 1:Active,2:In-active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday query()
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UpcomingHoliday whereYear($value)
 * @mixin \Eloquent
 */
class UpcomingHoliday extends Model
{
    use HasFactory;

    protected $fillable = [
      'date','name', 'year','status'
    ];
}
