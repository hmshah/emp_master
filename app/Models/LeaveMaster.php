<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LeaveMaster
 *
 * @property int $id
 * @property string $leave_type
 * @property int $status 1:Active,2:In-active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster query()
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster whereLeaveType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $total_leave
 * @method static \Illuminate\Database\Eloquent\Builder|LeaveMaster whereTotalLeave($value)
 */
class LeaveMaster extends Model
{
    use HasFactory;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
      'leave_type','total_leave','status'
    ];

    public static function LeaveType($id=null)
    {
        $leaveApplyTypes = [
            1 => 'Paid Leave',
            2 => 'Sick Leave',
            3 => 'Loss Of pay',
            4 => 'Covid Leave',
        ];

        if (isset($id)){
            return $leaveApplyTypes[$id];
        }else{
            return $leaveApplyTypes;
        }
    }
}
