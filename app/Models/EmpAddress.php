<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmpAddress
 *
 * @property int $id
 * @property int $employee_id
 * @property string $city
 * @property int $state_id
 * @property string $district
 * @property string $pin_code
 * @property string $address
 * @property int $country_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Country|null $country
 * @property-read \App\Models\Employee|null $employee
 * @property-read \App\Models\State|null $state
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress wherePinCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpAddress whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmpAddress extends Model
{
    use HasFactory;

    protected $fillable = [
          'employee_id','city','state_id','district','pin_code','address','country_id'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class,'employee_id','id');
    }

    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class,'country_id');
    }
}
