<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmpEducation
 *
 * @property int $id
 * @property int $emp_id
 * @property string|null $graduation
 * @property string|null $institute_name
 * @property string|null $start_at
 * @property string|null $end_at
 * @property int|null $state_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\State|null $state
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereEmpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereGraduation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereInstituteName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmpEducation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmpEducation extends Model
{
    use HasFactory;

    protected $fillable = [
      'emp_id', 'graduation', 'institute_name', 'start_at', 'end_at', 'state_id'
    ];

    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
}
