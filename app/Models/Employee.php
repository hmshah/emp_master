<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Employee
 *
 * @property int $id
 * @property string $emp_id
 * @property string $title
 * @property string $name
 * @property string $password
 * @property string $designation
 * @property int $gender 1:Male,2:Fe-male
 * @property string $email_id
 * @property int $salary
 * @property string $address
 * @property string $contact_no
 * @property int $nationality
 * @property string $birthday
 * @property string $joining_date
 * @property string|null $last_logged_in_ip
 * @property string|null $last_logged_in_at
 * @property int $emp_type 1:internship,2:permanent,3:part_time,4:freelancing
 * @property int $marital_status 1:Single,2:Married,3:Divorced
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereContactNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereDesignation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereEmailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereEmpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereEmpType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereJoiningDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereLastLoggedInAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereLastLoggedInIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereMaritalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $department_id 1:Marketing_department,2:Finance_department,3:Sales_department,4:Human Resource_department,5:IT_department
 * @property int $emp_position 1:Jr,2:Sr,3:TL,4:Manager
 * @property int $emp_access_mode 1:Admin,2:Employee
 * @property int|null $emp_status 1:Active,2:Terminate
 * @property-read \App\Models\EmpAddress|null $empAddDetails
 * @property-read \App\Models\Department|null $empDepartment
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereEmpAccessMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereEmpPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereEmpStatus($value)
 */
class Employee extends Model
{
    use HasFactory;

    const ADMIN = 1,EMPLOYEE = 2;
    const ACTIVE = 1,INACTIVE = 2;
    const JR = 1,Sr = 2,TL =3,Manager = 4;

    protected $fillable = [
          'emp_id','title','name','password','designation','department_id','gender','email_id','salary','address','contact_no','nationality','birthday','joining_date','emp_type','emp_position','emp_access_mode','marital_status','emp_status','last_logged_in_ip','last_logged_in_at'
    ];

    public function empAddDetails()
    {
        return $this->hasOne(EmpAddress::class,'employee_id');
    }

    public function empDepartment()
    {
        return $this->belongsTo(Department::class,'department_id');
    }

    public static function getEmpNameByDept($dept_id,$emp_id)
    {
        $emp = Employee::with(['empAddDetails','empDepartment'])->where('department_id',$dept_id)->where('id',$emp_id)->first();
        return $emp->name;
    }

    public static function getEmpName($empId)
    {
        $emp = Employee::whereId($empId)->first();
        return $emp->name;
    }

    public static function deptPosition($depPosId)
    {
        $data = [
            1 => 'JR',
            2 => 'Sr',
            3 => 'TL',
            4 => 'Manager',
            5 => 'Director'
        ];

        return isset($depPosId) ? $data[$depPosId] : '--';
    }


    public static function leaveAssignName($id)
    {
        $getdata = self::whereId($id)->first();

        return $getdata->name.'('.self::deptPosition($getdata->emp_position).')';
    }
}
