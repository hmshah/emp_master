<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TaskAssignMgt
 *
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $task_master_id
 * @property int $emp_id
 * @property int $status 1:In-queue,2:Staging,3:Testing,4:Staging,5:Finished
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt whereEmpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt whereTaskMasterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TaskAssignMgt whereUpdatedAt($value)
 */
class TaskAssignMgt extends Model
{
    use HasFactory;

    protected $fillable = [
        'task_master_id','emp_id','status'
    ];

    public function empTaskMaster()
    {
        return $this->belongsTo(EmpTaskMaster::class,'task_master_id','id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class,'emp_id','id');
    }
}
