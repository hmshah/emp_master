<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_leave_balance_management', function (Blueprint $table) {
            $table->id();
            $table->integer('emp_leave_master_id')->index();
            $table->integer('total_leave');
            $table->integer('status')->default(1)->comment('1:Active,2:InActive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_leave_balance_management');
    }
};
