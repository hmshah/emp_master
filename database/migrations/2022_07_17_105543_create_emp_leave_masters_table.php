<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_leave_masters', function (Blueprint $table) {
            $table->id();
            $table->integer('emp_id')->index();
            $table->integer('dept_id')->index();
            $table->integer('leave_type_id')->index();
            $table->integer('leave_assign_id')->index()->comment('This column use for assign Leave to TL/Manager');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->float('total_leave');
            $table->tinyInteger('leave_apply_type')->comment('1:Full_day,2:Half_day');
            $table->tinyInteger('status')->default('1')->comment('1:Pending,2:Approve,3:Reject');
            $table->tinyInteger('leave_status')->default('1')->comment('1:Credit,2:Debit,3:Hold');
            $table->string('image')->nullable();
            $table->text('reason');
            $table->dateTime('approved_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_leave_masters');
    }
};
