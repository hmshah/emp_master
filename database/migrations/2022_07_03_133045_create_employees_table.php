<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id');
            $table->string('title');
            $table->string('name');
            $table->string('password');
            $table->string('designation');
            $table->tinyInteger('department_id')->comment('1:Marketing_department,2:Finance_department,3:Sales_department,4:Human Resource_department,5:IT_department');
            $table->tinyInteger('gender')->comment('1:Male,2:Fe-male');
            $table->string('email_id');
            $table->integer('salary');
            $table->text('address');
            $table->string('contact_no');
            $table->integer('nationality');
            $table->date('birthday');
            $table->date('joining_date');
            $table->string('last_logged_in_ip')->nullable();
            $table->dateTime('last_logged_in_at')->nullable();
            $table->tinyInteger('emp_position')->comment('1:Jr,2:Sr,3:TL,4:Manager');
            $table->tinyInteger('emp_access_mode')->default(1)->comment('1:Admin,2:Employee');
            $table->tinyInteger('emp_type')->comment('1:internship,2:permanent,3:part_time,4:freelancing');
            $table->tinyInteger('emp_status')->comment('1:Active,2:Terminate');
            $table->tinyInteger('marital_status')->comment('1:Single,2:Married,3:Divorced');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
