<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_task_masters', function (Blueprint $table) {
            $table->id();
            $table->integer('emp_id');
            $table->integer('dept_id');
            $table->integer('created_by');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('description');
            $table->string('subject');
            $table->tinyInteger('status')->default('1')->comment('1:active','2.in-active');;
            $table->tinyInteger('task_status')->default('1')->comment('1:hold','2:in-process','3:tricky','4:completed');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_task_masters');
    }
};
