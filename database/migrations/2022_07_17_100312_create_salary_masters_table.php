<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_masters', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id')->index();
            $table->string('total',20);
            $table->string('basic_salary',20);
            $table->string('hra',20);
            $table->string('conveyance',20);
            $table->string('medical_allowance',20);
            $table->string('other_allowance',20);
            $table->string('advance_statutory_bonus',20);
            $table->string('generated_month',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_masters');
    }
};
