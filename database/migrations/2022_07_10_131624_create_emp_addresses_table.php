<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emp_addresses', function (Blueprint $table) {
            $table->id();
            $table->integer('employee_id')->index()->unique();
            $table->string('city');
            $table->integer('state_id');
            $table->string('district');
            $table->string('pin_code');
            $table->text('address');
            $table->integer('country_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_addresses');
    }
};
