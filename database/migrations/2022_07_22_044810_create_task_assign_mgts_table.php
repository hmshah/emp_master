<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_assign_mgts', function (Blueprint $table) {
            $table->id();
            $table->integer('task_master_id');
            $table->integer('emp_id');
            $table->tinyInteger('status')->comment('1:In-queue,2:Staging,3:Testing,4:Staging,5:Finished');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_assign_mgts');
    }
};
