<?php
Route::group(['namespace' => 'Employee'], function () {

    Route::get('login', [\App\Http\Controllers\Employee\LoginController::class, 'index'])->name('emp-login');
    Route::post('login', [\App\Http\Controllers\Employee\LoginController::class, 'index'])->name('emp-login');

    Route::get('forgot-password', [\App\Http\Controllers\Employee\LoginController::class, 'forgetPassword'])->name('emp-forget-password');
    Route::post('forgot-password', [\App\Http\Controllers\Employee\LoginController::class, 'forgetPassword'])->name('emp-forget-password');

    Route::get('/states/{id}', [\App\Http\Controllers\Employee\RegisterController::class, 'getStates'])->name('emp-register-status');

    Route::get('register', [\App\Http\Controllers\Employee\RegisterController::class, 'register'])->name('emp-register');
    Route::post('register', [\App\Http\Controllers\Employee\RegisterController::class, 'register'])->name('emp-register');

    Route::get('thanks', [\App\Http\Controllers\Employee\RegisterController::class, 'thanks'])->name('emp-register-thanks');


    Route::group(['middleware' => 'employee.auth'], function () {
        Route::get('dashboard', [\App\Http\Controllers\Employee\DashboardController::class,'index'])->name('emp-dashboard');

        // Profile page
        Route::get('profile', [\App\Http\Controllers\Employee\ProfileController::class,'index'])->name('emp-account-profile');

        //Salary slip view page
        Route::get('salary', [\App\Http\Controllers\Employee\SalaryController::class,'index'])->name('emp-salary-view');
        Route::post('salary', [\App\Http\Controllers\Employee\SalaryController::class,'index'])->name('emp-salary-view');

//        Generate All employee salary slip route
        Route::post('Salary-generate', [\App\Http\Controllers\Employee\SalaryController::class,'salaryGenerate'])->name('emp-salary-generate');

        // Download the Invoice route
        Route::get('download-salary/{download}', [\App\Http\Controllers\Employee\SalaryController::class,'download'])->name('emp-salary-download');

        // Leave Routes
        Route::get('leave', [\App\Http\Controllers\Employee\LeaveController::class,'index'])->name('emp-leave-view');

        Route::get('leave-create', [\App\Http\Controllers\Employee\LeaveController::class,'create'])->name('emp-leave-create');
        Route::post('leave-create', [\App\Http\Controllers\Employee\LeaveController::class,'create'])->name('emp-leave-create');

        // Leave Balance Route
        Route::get('leave-balance', [\App\Http\Controllers\Employee\LeaveController::class,'balance'])->name('emp-leave-balance');

        Route::get('leave-request-view', [\App\Http\Controllers\Employee\LeaveController::class,'requestLeaveView'])->name('emp-leave-request-view');

        Route::get('leave-request-edit/{id}', [\App\Http\Controllers\Employee\LeaveController::class,'leaveRequestEdit'])->name('emp-leave-request-approve');
        Route::post('leave-request-edit/{id}', [\App\Http\Controllers\Employee\LeaveController::class,'UpdateLeaveStatus'])->name('emp-leave-request-approve');

        //department Component
        Route::get('department-view', [\App\Http\Controllers\Employee\DepartmentController::class,'index'])->name('emp-department-view');

        Route::get('department-edit/{id}', [\App\Http\Controllers\Employee\DepartmentController::class,'edit'])->name('emp-department-edit');

        //This Route For check the attendance
        Route::get('attendance-approve', [\App\Http\Controllers\Employee\AttendanceController::class,'attendanceApprove'])->name('emp-leave-approve');

        Route::get('swipe-login', [\App\Http\Controllers\Employee\AttendanceController::class,'dailySwipe'])->name('emp-swipe-in-out');
        Route::post('swipe-login', [\App\Http\Controllers\Employee\AttendanceController::class,'dailySwipe'])->name('emp-swipe-in-out');

        //task

        Route::get('employee-task-view', [\App\Http\Controllers\Employee\TaskController::class,'index'])->name('emp-task-lists');

        Route::get('employee-task-create', [\App\Http\Controllers\Employee\TaskController::class,'create'])->name('emp-task-create');
        Route::post('employee-task-create', [\App\Http\Controllers\Employee\TaskController::class,'create'])->name('emp-task-create');

        Route::get('employee-task-update/{id}', [\App\Http\Controllers\Employee\TaskController::class,'update'])->name('emp-task-update');
        Route::post('employee-task-update/{id}', [\App\Http\Controllers\Employee\TaskController::class,'update'])->name('emp-task-update');

        //history
        Route::get('employee-task-history', [\App\Http\Controllers\Employee\TaskController::class,'taskHistory'])->name('emp-task-history');

        Route::get('get-employee/{id}', [\App\Http\Controllers\Employee\TaskController::class,'getEmployeeDptWise']);

        Route::get('form-test', [\App\Http\Controllers\Employee\TaskController::class,'testForm']);
        Route::get('form-records-store', [\App\Http\Controllers\Employee\TaskController::class,'FormDataStore']);

        // This route for logout
        Route::get('logout', [\App\Http\Controllers\Employee\LoginController::class,'logout'])->name('emp-logout');



        Route::resource('department', '\App\Http\Controllers\Employee\DepartmentController');
    });
});
