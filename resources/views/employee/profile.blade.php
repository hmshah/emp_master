@extends('employee.template.layout')

@section('title', 'My Profile')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Profile</h3>
        </div>
    </div>
    <div class="content-body">

        <div class="row offset-4">
            @if (session('errors'))
                <div class="alert alert-danger">
                    @foreach (session('errors')->all() as $error)
                        <span class="text-center">{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>



        <form role="form" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-square-controls">Personal Information</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Username (Employee Id)</label>
                                            <p>{{ $emp->name.' ('.$emp->emp_id.')' }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date of Birth</label>
                                            <input type="text" class="form-control square" name="birth_date" value="{{ Carbon\Carbon::parse($emp->birth_date)->format('d-m-Y') }}" readonly="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control square" name="emp_name" value="{{ $emp->name }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Mobile Number</label>
                                            <input type="text" name="contact_no" class="form-control square" placeholder="Enter Your Mobile Number"  value="{{ $emp->contact_no }}" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <fieldset class="form-group">
                                            <label>Marital Status</label>
                                            <select name="marital_status" id="marital_status" class="form-control square">
                                                <option value="">Select Status</option>
                                                <option value="1" {{$emp->marital_status == 1 ? 'selected' : ''}}>Single</option>
                                                <option value="2" {{$emp->marital_status == 2 ? 'selected' : ''}}>Married</option>
                                                <option value="3" {{$emp->marital_status == 3 ? 'selected' : ''}}>Divorced</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Department</label>
                                            <input type="text" name="department" class="form-control square" value="{{ isset($emp->department_id) ? $departments[$emp->department_id] : 'N.A'}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <select name="gender" id="gender" class="form-control square">
                                                <option value="">Select gender</option>
                                                <option value="1" {{$emp->gender == 1 ? 'selected' : ''}}>Male</option>
                                                <option value="2" {{$emp->gender == 2 ? 'selected' : ''}}>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email Id</label>
                                            <input type="text" name="email_id" class="form-control square" placeholder="Enter Your Pan Number"  value="{{ $emp->email_id }}" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-square-controls">Address Information</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <label>Address</label>
                                        @if(isset($emp->empAddDetails->address))
                                            <textarea type="text" class="form-control square" placeholder="Address" name="address" value="{{ $emp->empAddDetails->address}}">{{ $emp->empAddDetails->address}}</textarea>
                                        @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>District</label>
                                            @if(isset($emp->empAddDetails->district))
                                                <input type="text" class="form-control square" placeholder="Enter Landmark" name="landmark" value="{{ $emp->empAddDetails->district}}">
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label>City</label>
                                        @if(isset($emp->empAddDetails->city))
                                            <input type="tel" class="form-control square" name="city" value="{{ $emp->empAddDetails->city}}">
                                        @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="form-control" name="state_id">
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }}" {{ $emp->empAddDetails->state_id == $state->id ? 'selected' : ''}} >{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Pincode</label>
                                            @if(isset($emp->empAddDetails->pin_code))
                                                <input type="text" class="form-control square" placeholder="Pincode" name="pin_code" value="{{ $emp->empAddDetails->pin_code}}">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-square-controls">Education Information</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Graduation</label>
                                        <input type="text" class="form-control square" placeholder="Graduation" name="graduation" value="" >
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input type="text" class="form-control square" placeholder="institute_name" name="institute_name" value="">
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Time duration of Graduation</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                                <input type="text" name="duration_of_graduation" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="form-control" name="grad_state_id">
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }}" {{ $emp->empAddDetails->state_id == $state->id ? 'selected' : ''}} >{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-t-30 m-b-20 offset-5 text-center">
                <button type="submit" class="btn btn-danger">Update</button>
            </div>
        </form>
    </div>
@stop

@section('page-javascript')
    <script>
        $(".birth-date").pickadate({
            selectMonths: !0,
            selectYears: 50,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->format('Y,m,d') }}')
        });
    </script>

@stop