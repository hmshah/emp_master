@extends('employee.template.layout')

@section('title', 'Department Update')

@section('content')
    <div class="row">
        <div class="col-8 offset-2">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title text-center">Update Department Leads</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                    <div class="card-content collapse show">
                        <form action="{{route('department.update',[$department->id])}}" method="post">
                            @csrf
                            @method('put')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Department:</label>
                                            <input type="text" class="form-control square" name="department_name" value="{{ $department->dept_name }}" readonly>
                                        </div>
                                    </div>
                                    {{--<div class="col-md-12">--}}
                                        {{--<fieldset class="form-group">--}}
                                            {{--<label>Department Type :</label>--}}
                                            {{--<select name="leave_type_id" class="form-control square" disabled>--}}
                                                {{--<option value="">Select Department type</option>--}}
                                                {{--@foreach(\App\Models\Department::departments() as $key => $departmentName)--}}
                                                    {{--<option value="{{$key}}" {{$key == $department->id ? 'selected' : ''}} disabled>{{$departmentName}}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        {{--</fieldset>--}}
                                    {{--</div>--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Assign TL to Department:</label>
                                            <select name="select_tl_id" class="form-control square">
                                                <option value="">Select Lead</option>
                                                @foreach($departmentEmp as $key => $employee)
                                                    <option value="{{$key}}" {{$department->teamLeadId->id == $key ? 'selected' : ''}}>{{$employee}} {{$department->teamLeadId->id == $key ? "(Current TL)" : ""}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Assign Manager to Department:</label>
                                            <select name="select_mgr_id" class="form-control square">
                                                <option value="">Select Manager</option>
                                                @foreach($departmentEmp as $key => $employee)
                                                    <option value="{{$key}}" {{$department->deptMgrId->id == $key ? 'selected' : ''}}>{{$employee}} {{$department->deptMgrId->id == $key ? "(Current Manager)" : ""}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input class="btn btn-primary text-center" type="submit" value="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
