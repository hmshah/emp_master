@extends('employee.template.layout')

@section('title', 'Department View')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="row  col-md-12">
                    <div class="card-header col-md-6">
                        <h4 class="card-title">Department List</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="text-right mt-2 col-md-6">
                        <a href="{{route('department.create')}}" class="btn btn-sm btn-bg-gradient-x-red-pink text-right">Create</a>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary white">
                            <tr>
                                <th>#</th>
                                <th>Department Id</th>
                                <th>Department</th>
                                <th>Team Leader Name</th>
                                <th>Manager Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($departments as $index => $department)
                                <tr>
                                    <td>{{ $index+1}}</td>
                                    <td scope="row">{{$department->dept_no}}</td>
                                    <td>{{$department->dept_name}}</td>
                                    <td>{{$department->teamLeadId->name}}</td>
                                    <td>{{$department->deptMgrId->name}}</td>
                                    <td>
                                        <a href="{{route('emp-department-edit',['id' => $department->id])}}" class="btn btn-bg-gradient-x-purple-blue btn-sm">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
