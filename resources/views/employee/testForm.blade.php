@extends('employee.template.registration.layout')

@section('title', 'Member / User Details for Registration')

@section('content')
    <div class="pt-2 pb-3">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-8 col-12 box-shadow-2 p-0">
                <form action="" method="post" id="formSubmit">
                    {{ csrf_field() }}
                    <div class="card border-grey border-lighten-3 px-1 py-1">
                        <div class="card-header border-0">
                            <div class="text-center mb-0">
                                <p style="font-size: x-large; font-style: normal; font-weight: 600;">Hello there!</p> <span style="font-size: 8rem;color:#fff;background-image:-webkit-gradient(linear,left top,right top,from(#514a9d),to(#24c6dc));border-radius: 20px;" class="la la-hands-helping"></span>
                            </div>
                            @if(session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @elseif(session('errors'))
                                <div class="alert alert-danger">{{ session('errors')->first() }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div class="card-header border-0">
                            <div class="font-medium-4 text-center">
                                Enter New Details
                            </div>
                            <hr>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>Salutation Name</label>
                                        <input type="text" class="form-control square" style="text-transform: uppercase" name="salutationName" placeholder="Salutation Name" value="{{ old('salutation_name') }}" autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control square" style="text-transform: uppercase" name="firstName" placeholder="First Name" value="{{ old('firstName') }}" autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control square" style="text-transform: uppercase" name="lastName" placeholder="Last Name" value="{{ old('lastName') }}" autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>middle Name</label>
                                        <input type="text" class="form-control square" style="text-transform: uppercase" name="middleName" placeholder="Last Name" value="{{ old('middleName') }}" autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Email Address</label>
                                        <input type="text" class="form-control square" onkeypress="INGENIOUS.numericInput(event)" name="emailAddress" value="{{ old('emailAddress') }}" placeholder="10 Digit Mobile Number" autocomplete="new-mobile">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control square" name="phoneNumber" value="{{ old('phoneNumber') }}" placeholder="Enter your Designation">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>description</label>
                                        <input type="email" class="form-control square" name="description" value="{{ old('description') }}" placeholder="Enter Email Ids">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Account Name :</label>
                                        <input type="text" class="form-control square" name="accountName" value="{{ old('accountName') }}" placeholder="Enter Salary here">
                                    </fieldset>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 mt-1">
                                    <div class="checkbox check-danger">
                                        <input type="checkbox" id="terms" name="terms" required>
                                        <label for="terms">
                                            I agree to the Terms and Conditions and Privacy Policy For {{ config('project.company') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mt-1">
                                    <button type="button" class="btn round btn-glow btn-bg-gradient-x-red-pink col-6 mr-1 mb-1" id="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $(".birth-date").pickadate({
            selectMonths: !0,
            selectYears: 50,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->subYears(0)->format('Y,m,d') }}')
        });
        $(".joining_date").pickadate({
            selectMonths: !0,
            selectYears: 50,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->subYears(0)->format('Y,m,d') }}')
        });
    </script>
    <script>
        $("#submit").click(function () {
            var form = $("#formSubmit").serializeArray();
            $('input[name=baz]:checked').val();
            console.log('hi',this.form);
            var url = $(location).attr('href');
            $('#spn_url').html('<strong>' + url + '</strong>');
        });
        $('#nationality').change(function () {
            var country_id = $("#nationality").val();
            $.ajax({
                url: "states/country_id",
                type:'get',
                data:{country_id},
                success: function(data){
                    $('#state_id').empty();
                    $('#state_id').append("<option value=''>Select State</option>");
                    $.each(data, function(key, value) {
                        $('#state_id').append("<option value='" + value.id + "'>" + value.name + "</option>");
                    });
                }});
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@stop
