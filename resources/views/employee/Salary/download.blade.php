<html>
<head>
    <style>
        table{
            width: 100%;
            border-collapse:collapse;
            border: 1px solid black;
        }
        table td{line-height:25px;padding-left:15px;}
        table th{background-color:#fbc403; color:#363636;}
    </style>

</head>
<body>
<table border="1">
    <tr height="100px" style="color:#000;text-align:center;font-size:24px; font-weight:600;">
        <td colspan='4'>Employee Salary Slip</td>
    </tr>
    <tr>
       <td colspan="4" style="text-align: center">Salary Slip Of {{ $salaryDetail->generated_month }}</td>
    </tr>
    <tr>
        <th>Emp Id:</th>
        <td>{{$employee->emp_id}}</td>
        <th>Name</th>
        <td>{{$employee->name}}</td>
    </tr>
    <!-----2 row--->
    <tr>
        <th>Date Of Joining</th>
        <td>{{$employee->joining_date}}</td>
        <th>Designation</th>
        <td>{{$employee->designation}}</td>
    </tr>
    <!------3 row---->
    <tr>
        <th>DOB</th>
        <td>{{$employee->birthday}}</td>
        <th>Email Id</th>
        <td>{{$employee->email_id}}</td>
    </tr>
    <!------4 row---->
    <tr>
        <th>PF No.</th>
        <td>26123456</td>
        <th>Location</th>
        <td>{{ \App\Models\Country::countryName($employee->nationality) }}</td>
    </tr>
    <!------5 row---->
    <tr>
        <th>Department</th>
        <td>IT</td>
        <th>Working Days</th>
        <td>30</td>
    </tr>
</table>
<tr></tr>
<br/>
<table border="1">
    <tr>
        <th >Earnings</th>
        <th>Amount</th>
        <th >Deductions</th>
        <th>Amount</th>
    </tr>
    <tr>
        <td>Basic</td>
        <td>{{$salaryDetail->basic_salary}}</td>
        <td>Prof. Tax</td>
        <td>{{$deduction->professional_tax}}</td>
    </tr>
    <tr>
        <td>HRA</td>
        <td>{{$salaryDetail->hra}}</td>
        <td>PF</td>
        <td>{{$deduction->pf_deduction}}</td>
    </tr>
    <tr>
        <td>Other Allowance</td>
        <td> {{$salaryDetail->other_allowance}}</td>
        <td>Mediclaim Deduction</td>
        <td>{{$deduction->mediclaim_deduction}}</td>
    </tr>
    <tr>
        <td>Conveyance</td>
        <td>{{$salaryDetail->conveyance}}</td>
        <td>Leave Deduction</td>
        <td>{{$deduction->Leave_deduction}}</td>
    </tr>
    <tr>
        <td>Medical Allowance</td>
        <td>{{$salaryDetail->medical_allowance}}</td>
    </tr>
    <tr>
        <td><strong>Total</strong></td>
        <td>{{$salaryDetail->total}}</td>
        <td><strong>NET PAY</strong></td>
        <td>
            {{$salaryDetail->total - $totalDeduction}}
        </td>
    </tr>
    <tr>

    </tr>
</table>

<table style="margin-top: 20px">
    <tr>
        <td>
            <p style="text-align: center">
                This is a Computer Generated Order Receipt
            </p>
        </td>
    </tr>
</table>
</body>
</html>