@extends('employee.template.layout')

@section('title', 'Salary')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Salary Slip</h3>
        </div>

        <div class="col-md-8">
            <form action="">
                <div class="row pull-right">
                    <div class="col-md-6 col-12 mb-2">
                        <input type="text" name="select_month" class="form-control select-month" value="{{ Request::get('dateRange') }}" placeholder="Select Month" readonly>
                    </div>
                    <div class="col-md-2 col-12 mb-2">
                        <input type="submit" name="search" class="btn btn-bg-gradient-x-orange-yellow" value="Search">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="content-body">

        <div class="row offset-4">
            @if (session('errors'))
                <div class="alert alert-danger">
                    @foreach (session('errors')->all() as $error)
                        <span class="text-center">{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
        <form role="form" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title text-center" id="basic-layout-square-controls">PaySlip OF {{$salaryDetail->generated_month}}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-square-controls">Earning</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content">
                            <div id="recent-projects" class="media-list position-relative">
                                <div class="table-responsive">
                                    <table class="table table-padded table-xl mb-0" id="recent-project-table">
                                        <thead class="bg-gradient-x-blue-purple-1 white">
                                        <tr>
                                            <th class="border-top-0">Type</th>
                                            <th class="border-top-0">Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-truncate align-middle">
                                                <a href="#">Basic</a>
                                            </td>
                                            <td>
                                                <div>
                                                    {{$salaryDetail->basic_salary}}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate align-middle">
                                                <a href="#">HRA</a>
                                            </td>
                                            <td>
                                                {{$salaryDetail->hra}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate align-middle">
                                                <a href="#">Conveyance</a>
                                            </td>
                                            <td>
                                                <div>
                                                    {{$salaryDetail->conveyance}}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate align-middle">
                                                <a href="#">Medical Allowance</a>
                                            </td>
                                            <td>
                                                <div>
                                                    {{$salaryDetail->medical_allowance}}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate align-middle">
                                                <a href="#">Other Allowance</a>
                                            </td>
                                            <td>
                                                <div>
                                                    {{$salaryDetail->other_allowance}}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-truncate align-middle">
                                                <a href="#">Total</a>
                                            </td>
                                            <td>
                                                <div>
                                                    {{$salaryDetail->total}}
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">

                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-square-controls">Deduction</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <div class="card-content">
                            <div id="recent-projects" class="media-list position-relative">
                                <div class="table-responsive">
                                    <table class="table table-padded table-xl mb-0" id="recent-project-table">
                                        <thead class="bg-gradient-x-blue-purple-1 white">
                                        <tr>
                                            <th class="border-top-0">Type</th>
                                            <th class="border-top-0">Deduction</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-truncate align-middle">
                                                Professional Tax
                                            </td>
                                            <td>
                                                <div>{{$deduction->professional_tax}}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                PF
                                            </td>
                                            <td>
                                                <div> {{$deduction->pf_deduction}} </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Mediclaim Deduction
                                            </td>
                                            <td>
                                                <div> {{$deduction->mediclaim_deduction}} </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Leave Deduction
                                            </td>
                                            <td>
                                                <div> {{$deduction->Leave_deduction}} </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Total
                                            </td>
                                            <td>
                                                <div> {{$salaryDetail->total - $totalDeduction}} </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Download PDF
                                            </td>
                                            <td>
                                                <a href="{{route('emp-salary-download',['download' => 'yes','month' => $salaryDetail->generated_month ])}}" class="btn btn-info btn-sm">
                                                    <i class="la la-cloud-download"></i>
                                                    <span class="bold">Download</span>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('page-javascript')
    <script>
        $(".select-month").datepicker( {
            format: "M-yyyy",
            startView: "months",
            minViewMode: "months"
        });
    </script>

@stop