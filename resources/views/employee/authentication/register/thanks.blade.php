@extends('employee.template.registration.layout')

@section('title', 'Registration Completed')

@section('content')
    <section class="flexbox-container">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-10 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                    <div class="card-header border-0">
                        <div class="text-center mb-1">
                            <p style="font-size: x-large; font-style: normal; font-weight: 600;">Hello there!</p>
                            <span style="font-size: 8rem;color:#fff;background-image:-webkit-gradient(linear,left top,right top,from(#514a9d),to(#24c6dc));border-radius: 20px;" class="la la-hands-helping">
                            </span>
                        </div>
                        @if(session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @endif
{{--                        {{dd($employee)}}--}}
                        {{--{{dd($employee)}}--}}
                        <h2 class="text-center">Welcome {{$employee->name}}</h2>
                    </div>
                    <div class="card-content">

                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item">

                                   Employee Name: <span class="pull-right">{{ $employee->name }}</span>
                                </li>
                                <li class="list-group-item bg-dark text-white">
                                    Employee ID: <span class="pull-right">{{ $employee->emp_id }}</span>
                                </li>
                            </ul>
                            <div class="text-center mt-2">
                                <a href="{{ route('emp-login') }}" class="btn btn-glow btn-bg-gradient-x-red-pink mr-1 mb-1 continue">
                                    Visit Your Account <i class="la la-arrow-circle-o-right"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@stop