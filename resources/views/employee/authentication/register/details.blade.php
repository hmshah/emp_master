@extends('employee.template.registration.layout')

@section('title', 'Member / User Details for Registration')

@section('content')
    <div class="pt-2 pb-3">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-8 col-12 box-shadow-2 p-0">
                <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="card border-grey border-lighten-3 px-1 py-1">
                        <div class="card-header border-0">
                            <div class="text-center mb-0">
                                <p style="font-size: x-large; font-style: normal; font-weight: 600;">Hello there!</p> <span style="font-size: 8rem;color:#fff;background-image:-webkit-gradient(linear,left top,right top,from(#514a9d),to(#24c6dc));border-radius: 20px;" class="la la-hands-helping"></span>
                            </div>
                            @if(session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @elseif(session('errors'))
                                <div class="alert alert-danger">{{ session('errors')->first() }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div class="card-header border-0">
                            <div class="font-medium-4 text-center">
                                Enter New Employee Details
                            </div>
                            <hr>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <fieldset class="form-group">
                                        <label>Title</label>
                                        <select name="title" class="form-control square">
                                            <option value="Mr" {{ old('title') == 'Mr' ? 'selected' : null }}>Mr</option>
                                            <option value="Miss" {{ old('title') == 'Miss' ? 'selected' : null }}>Miss</option>
                                            <option value="Mrs" {{ old('title') == 'Mrs' ? 'selected' : null }}>Mrs</option>
                                            <option value="Mrs" {{ old('title') == 'Ms' ? 'selected' : null }}>Ms</option>
                                            <option value="Dr" {{ old('title') == 'Dr' ? 'selected' : null }}>Dr</option>
                                            <option value="Sri" {{ old('title') == 'Sri' ? 'selected' : null }}>Sri</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control square" style="text-transform: uppercase" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control square" style="text-transform: uppercase" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Mobile Number</label>
                                        <input type="text" class="form-control square" onkeypress="INGENIOUS.numericInput(event)" name="mobile" value="{{ old('mobile') }}" placeholder="10 Digit Mobile Number" autocomplete="new-mobile">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <label>Birth Date</label>
                                    <div class="input-group">
                                        <input type='text' class="form-control square birth-date" name="birth_date" value="{{ old('birth_date') }}" placeholder="Birth date" readonly>
                                        <div class="input-group-append">
										<span class="input-group-text">
											<span class="la la-calendar-o"></span>
										</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Designation</label>
                                        <input type="text" class="form-control square" name="designation" value="{{ old('designation') }}" placeholder="Enter your Designation">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Email Id :</label>
                                        <input type="email" class="form-control square" name="email_id" value="{{ old('email_id') }}" placeholder="Enter Email Ids">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Gender:</label>
                                        <select name="gender" class="form-control square">
                                            <option value="1">Male</option>
                                            <option value="2">Fe-Male</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <label>Password :</label>
                                        <div class="input-group mb-3">
                                            <input type="password" class="form-control passwordInput" name="password" placeholder="Enter Password">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button" onclick="INGENIOUS.showPassword(this)">
                                                    <i class="la la-eye-slash"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Salary :</label>
                                        <input type="text" class="form-control square" name="salary" value="{{ old('salary') }}" placeholder="Enter Salary here">
                                    </fieldset>
                                </div>

                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <fieldset class="form-group">
                                            <label>Nationality</label>
                                            <select name="nationality" id="nationality" class="form-control square">
                                                <option value="">Select Country</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </fieldset>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <fieldset class="form-group">
                                            <label>Marital Status</label>
                                            <select name="marital_status" id="marital_status" class="form-control square">
                                                <option value="">Select Status</option>
                                                <option value="1">Single</option>
                                                <option value="2">Married</option>
                                                <option value="3">Divorced</option>
                                            </select>
                                        </fieldset>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <fieldset class="form-group">
                                            <label>Employee Position</label>
                                            <select name="employee_position" class="form-control square">
                                                <option value="">Select Position</option>
                                                <option value="1">Jr.</option>
                                                <option value="2">Sr.</option>
                                                <option value="3">TL.</option>
                                                <option value="4">Manager.</option>
                                            </select>
                                        </fieldset>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <fieldset class="form-group">
                                            <label>Employee Access</label>
                                            <select name="employee_access" class="form-control square">
                                                <option value="">Select access</option>
                                                <option value="1">Admin Type</option>
                                                <option value="2">Employee Type</option>
                                            </select>
                                        </fieldset>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <fieldset class="form-group">
                                            <label>Employee Type</label>
                                            <select name="emp_type" class="form-control square">
                                                <option value="1" {{ old('emp_type') == 'internship' ? 'selected' : null }}>Internship</option>
                                                <option value="2" {{ old('emp_type') == 'permanent' ? 'selected' : null }}>Permanent</option>
                                                <option value="3" {{ old('emp_type') == 'Part_time' ? 'selected' : null }}>Part-time</option>
                                                <option value="4" {{ old('emp_type') == 'freelancing' ? 'selected' : null }}>Freelancing</option>
                                            </select>
                                        </fieldset>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <label>Joining Date</label>
                                    <div class="input-group">
                                        <input type='text' class="form-control square joining_date" name="joining_date" value="{{ old('joining_date') }}" placeholder="joining_date" readonly>
                                        <div class="input-group-append">
										<span class="input-group-text">
											<span class="la la-calendar-o"></span>
										</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <fieldset class="form-group">
                                            <label>Department</label>
                                            <select name="department_id" class="form-control square">
                                                <option value="">Select Department</option>
                                                @foreach($departments as $key => $department)
                                                    <option value="{{$key}}" {{ old('department_id') == $department ? 'selected' : null }}>{{$department}}</option>
                                                @endforeach
                                            </select>
                                        </fieldset>
                                    </fieldset>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Enter Address Details
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label>Your Address</label>
                                        <textarea class="form-control square" name="address" style="text-transform: uppercase" rows="6" placeholder="Enter Address">{{ old('address') }}</textarea>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control square" style="text-transform: uppercase" name="city" value="{{ old('city') }}" autocomplete="off">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>State</label>
                                                <select name="state_id" id="state_id" class="form-control square" required>
                                                    <option value="">Select State</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label>District</label>
                                                <input type="text" class="form-control square" style="text-transform: uppercase" name="district" value="{{ old('district') }}" autocomplete="off">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Pincode</label>
                                                <input type="text" class="form-control square" onkeypress="INGENIOUS.numericInput(event)" name="pincode" value="{{ old('pincode') }}" autocomplete="off">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 mt-1">
                                    <div class="checkbox check-danger">
                                        <input type="checkbox" id="terms" name="terms" required>
                                        <label for="terms">
                                            I agree to the Terms and Conditions and Privacy Policy For {{ config('project.company') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mt-1">
                                    <button type="submit" class="btn round btn-glow btn-bg-gradient-x-red-pink col-6 mr-1 mb-1">Continue</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $(".birth-date").pickadate({
            selectMonths: !0,
            selectYears: 50,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->subYears(0)->format('Y,m,d') }}')
        });
        $(".joining_date").pickadate({
            selectMonths: !0,
            selectYears: 50,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->subYears(0)->format('Y,m,d') }}')
        });
    </script>
    <script>
        $('#nationality').change(function () {
            var country_id = $("#nationality").val();
            $.ajax({
                url: "states/country_id",
                type:'get',
                data:{country_id},
                success: function(data){
                    $('#state_id').empty();
                    $('#state_id').append("<option value=''>Select State</option>");
                    $.each(data, function(key, value) {
                        $('#state_id').append("<option value='" + value.id + "'>" + value.name + "</option>");
                    });
                }});
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@stop
