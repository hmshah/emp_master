@extends('employee.template.layout')

@section('title', 'Task View')

@section('content')
    <div class="row">
        <div class="col-md-8 offset-2">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-square-controls">Task Information</h4>
                    <a class="heading-elements-toggle">
                        <i class="la la-ellipsis-v font-medium-3"></i>
                    </a>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                @if($getEmpTasks->count() == 0)
                                    <section id="collapsible">
                                        <div class="card">
                                            <div id="collapse1" class="card-collapse">
                                                <div class="card mb-0">
                                                    <div class="card-header" id="headingAOne">
                                                        <h5 class="mb-0 text-center">
                                                            <button class="btn btn-link text-center" data-toggle="collapse" data-target="#collapseAOne" aria-expanded="true" aria-controls="collapseAOne">
                                                                No Records Found here .....!!
                                                            </button>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                @endif
                                @foreach($getEmpTasks as $key => $getEmpTask)
                                    <section id="collapsible">
                                        <div class="card">
                                            <div id="collapse1" class="card-collapse">
                                                <div class="card mb-0">
                                                    <div class="card-header" id="headingA{{$key}}">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseA{{$key}}" aria-expanded="true" aria-controls="collapseA{{$key}}">
                                                                {{$key+1}}) {{$getEmpTask->subject}}
                                                            </button>
                                                            <a style="float: right" href="{{route('emp-task-update',['id' => $getEmpTask->id])}}">Edit</a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseA{{$key}}" class="collapse" aria-labelledby="headingA{{$key}}" style="">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-xl-6">
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item">
                                                                            <span class="badge badge-info float-right">{{\Carbon\Carbon::parse($getEmpTask->start_date)->format('Y-m-d')}} - {{\Carbon\Carbon::parse($getEmpTask->end_date)->format('Y-m-d')}}</span>
                                                                            Time Duration :
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                                <span class="badge badge-danger float-right">{{\App\Models\Employee::getEmpNameByDept($getEmpTask->dept_id, $getEmpTask->emp_id)}}</span>

                                                                            Task Created By :
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            @if($getEmpTask->status == 1)
                                                                                <span class="badge badge-danger float-right">Active</span>
                                                                            @elseif($getEmpTask->status == 2)
                                                                                <span class="badge badge-danger float-right">InActive</span>
                                                                            @endif
                                                                            Status :
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                                <div class="col-lg-6 col-xl-6">
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item">
                                                                            <span class="badge badge-info float-right">
                                                                                {{\App\Models\EmpTaskMaster::currentTaskStatus($getEmpTask->task_status)}}
                                                                            </span>
                                                                            Task Current Status :
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                                <span class="badge badge-info float-right">
                                                                                {{\App\Models\Department::departments($getEmpTask->dept_id)}}</span>
                                                                            Department Name :
                                                                        </li>
                                                                        <li class="list-group-item">
                                                                            <span class="badge badge-danger float-right">{{\App\Models\Employee::getEmpName($getEmpTask->emp_id)}}</span>
                                                                            Assign To :
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item">
                                                                            <span class="float-right">{{$getEmpTask->description}}</span>
                                                                            Task Description:
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
