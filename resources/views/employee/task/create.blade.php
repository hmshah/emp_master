@extends('employee.template.layout')

@section('title', 'Department Update')

@section('content')
    <div class="row">
        <div class="col-8 offset-2">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title text-center">Create Task </h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                    <div class="card-content collapse show">
                        <form action="{{route('emp-task-create')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Task Title:</label>
                                            <input type="text" class="form-control square" placeholder="Enter Task Title" name="task_subject">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Current Status :</label>
                                            <select name="task_current_status" class="form-control square">
                                                <option value="">Select Current Status :</option>
                                                @foreach(\App\Models\EmpTaskMaster::currentTaskStatus() as $key => $status)
                                                <option value="{{$key}}">{{$status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Department</label>
                                            <select name="select_dept_id" id="select_dept_id" class="form-control square">
                                                <option value="">Select Department</option>
                                                @foreach(\App\Models\Department::departments() as $key => $department)
                                                    <option value="{{$key}}">{{$department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Select Employee</label>
                                            <select name="emp_id" id="emp_id" class="form-control square">
                                                <option value="">Select Employee</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Task duration :</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                                <input type="text" name="task_duration" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Task duration" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Status :</label>
                                            <select name="status" class="form-control square">
                                                <option value="">Select Status</option>
                                                <option value="1">Active</option>
                                                <option value="2">InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description:</label>
                                            <textarea type="text" class="form-control square" rows="5" placeholder="Enter Description here" name="description"> </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input class="btn btn-primary text-center" type="submit" value="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-javascript')
    <script>
        $(".birth-date").pickadate({
            selectMonths: !0,
            selectYears: 50,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->subYears(0)->format('Y,m,d') }}')
        });
        $(".joining_date").pickadate({
            selectMonths: !0,
            selectYears: 50,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->subYears(0)->format('Y,m,d') }}')
        });
    </script>
    <script>
        $('#select_dept_id').change(function () {
            var deptId = $("#select_dept_id").val();
            $.ajax({
                url: "get-employee/deptId",
                type:'get',
                data:{deptId},
                success: function(data){
                    $('#emp_id').empty();
                    $('#emp_id').append("<option value=''>Select Employee</option>");
                    $.each(data, function(key, value) {
                        $('#emp_id').append("<option value='" + key + "'>" + value + "</option>");
                    });
                }});
        });
    </script>
@stop
