@extends('employee.template.layout')

@section('title', 'Dashboard')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Dashboard</h3>
        </div>
        <div class="content-header-left col-md-10 col-12 mb-2">
        </div>
    </div>

    <div class="content-body">
        <div class="row match-height">
            <div class="col-xl-4 col-lg-4 col-md-4">
                <a href="{{route('emp-salary-view')}}">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-money danger font-large-4"></i>
                                    </div>
                                    <div class="media-body text-right align-self-bottom mt-3">
                                        <span class="d-block mb-1 font-medium-1">salary</span>
                                        <h1 class="info mb-0">{{$totalSalarySlip}}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @if($employeeData->emp_access_mode == \App\Models\Employee::ADMIN)
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <a>
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-top">
                                            <i class="la la-money danger font-large-4"></i>
                                        </div>
                                        <div class="media-body text-right align-self-bottom mt-3">
                                            <span class="d-block mb-1 font-medium-1">Payroll</span>
                                            <p>Generate Payroll's Slip</p>
                                            <form action="{{route('emp-salary-generate')}}" method="post">
                                                @csrf
                                                <button class="btn btn-sm btn-bg-gradient-x-purple-red" type="submit" name="submit">Generate</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endif

            @if($totalReqData > 0)
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <a href="{{route('emp-leave-request-view')}}">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-top">
                                            <i class="la la-calendar danger font-large-4"></i>
                                        </div>
                                        <div class="media-body text-right align-self-bottom mt-3">
                                            <span class="d-block mb-1 font-medium-1">{{$totalReqData}}</span>
                                            <p>Total Pending Leave Request Exist</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endif
            <div class="col-xl-4 col-lg-4 col-md-4">
                <a href="javascript:void(0);">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="align-self-top">
                                        <i class="la la-fingerprint danger font-large-4"></i>
                                    </div>
                                    <div class="align-self-end">
                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
                                            View Swipe
                                        </button>
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Employee Swipe Details</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <ul class="list-group">
                                                                    @if(empty($getEmpSwipeData))
                                                                        <li class="list-group-item bg-gradient-x-blue-cyan text-white">
                                                                           <p class="text-center"> No Record Found Please Login First</p>
                                                                        </li>
                                                                    @else
                                                                        @foreach($getEmpSwipeData as $data)
                                                                            <li class="list-group-item">
                                                                                <span class="float-right btn btn-bg-gradient-x-red-pink btn-sm">{{\App\Models\AttendanceLog::swipeTypes($data->type)}}</span>
                                                                                {{\Carbon\Carbon::parse($data->log_time)->format('Y-m-d h:i:s')}}
                                                                            </li>
                                                                        @endforeach
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="media-body text-right align-self-bottom mt-3">
                                        <p id="time"></p>
                                        <form action="{{route('emp-swipe-in-out')}}" method="post">
                                            @csrf
                                            @if(!empty(\Session::get('emp_swipe')))
                                                @foreach(\Session::get('emp_swipe') as $empSwipe)
                                                    @if($empSwipe->type == 1)
                                                        <button class="btn btn-sm btn-bg-gradient-x-purple-red" type="submit" name="type" value="2">Logout</button>
                                                    @else
                                                        <button class="btn btn-sm btn-bg-gradient-x-purple-blue" type="submit" name="type" value="1">Login</button>
                                                    @endif
                                                @endforeach
                                            @else
                                                <button class="btn btn-sm btn-bg-gradient-x-purple-blue" type="submit" name="type" value="1">Login</button>
                                            @endif
                                        </form>
                                        <p>Daily Track Your Regularity</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-12">
                <div class="card" style="height: 340px;">
                    <div class="card-header">
                        <h4 class="card-title text-center">Upcoming Holidays</h4>
                    </div>
                    <hr>
                    <div class="card-content">
                        <div class="card-body">
                            <ul class="list-group">
                                @foreach($upcomingHolidays as $upcomingHoliday)
                                    <li class="list-group-item bg-gradient-x-blue-cyan text-white">
                                        <span class="float-right">{{$upcomingHoliday->name}}</span>
                                        {{\Carbon\Carbon::parse($upcomingHoliday->date)->format('d-m-Y')}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-12">
                <div class="card ">
                    <div class="card-header white bg-gradient-x-orange-yellow p-1">
                        <h4 class="card-title float-left white">My Details</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-footer pr-1 pl-1 pt-1">
                            <ul class="list-group">
                                <li class="list-group-item white bg-gradient-x-purple-red">
                                    <strong>Name</strong>
                                    <span class="pull-right">
                                        {{ $employeeData->name}}
                                    </span>
                                </li>
                                <li class="list-group-item white bg-gradient-x-purple-red">
                                    <strong>Employee Id</strong>
                                    <span class="pull-right">{{$employeeData->emp_id}}</span>
                                </li>
                                <li class="list-group-item white bg-gradient-x-purple-red">
                                    <strong>Department</strong>
                                    <span class="pull-right">{{ \App\Models\Department::empDepartment($employeeData->department_id) }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
