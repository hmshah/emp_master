@extends('employee.template.layout')

@section('title', 'Leave Apply')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Leave Status Update Form</h3>
        </div>
    </div>
    <div class="content-body">
        <form role="form" method="post" enctype="multipart/form-data" action="{{route('emp-leave-request-approve',$getRequestDetails->id)}}">
            {{ csrf_field() }}
            <div class="row">

                <div class="col-md-6 offset-3">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title text-center" id="basic-layout-square-controls">Apply Leave Status Update</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <hr>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="form-group">
                                            <h4><b>Username (Employee Id)</b></h4>
                                            <b>{{ $getRequestDetails->employee->name.' ('.$getRequestDetails->employee->emp_id.')' }}</b>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Name :</label>
                                            <input type="text" class="form-control square" name="emp_name" value="{{ $getRequestDetails->employee->name }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group">
                                            <label>Select Leave Type :</label>
                                            <select name="leave_type_id" class="form-control square" disabled>
                                                <option value="">Select Leave type</option>
                                                @foreach(\App\Models\LeaveMaster::LeaveType() as $key => $type)
                                                    <option value="{{$key}}" {{$key == $getRequestDetails->leave_type_id ? 'selected' : ''}} readonly>{{$type}}</option>
                                                @endforeach
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group">
                                            <label>Select Leave mode :</label>
                                            <select name="leave_apply_type" class="form-control square" disabled>
                                                <option value="">Select Leave type</option>
                                                <option value="1" {{$getRequestDetails->leave_apply_type == 1 ? 'selected' : ''}}>Full Day</option>
                                                <option value="2" {{$getRequestDetails->leave_apply_type == 2 ? 'selected' : ''}}>half Day</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Time duration For leave :</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                                <input type="text" name="Leave_duration" class="form-control" value="{{\Carbon\Carbon::parse($getRequestDetails->start_date)->format('d-m-Y').'-'.\Carbon\Carbon::parse($getRequestDetails->end_date)->format('d-m-Y') }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group">
                                            <label>Select Status:</label>
                                            <select name="leave_status" class="form-control square">
                                                <option value="">Select Status</option>
                                                <option value="1" {{$getRequestDetails->status == 1 ? 'selected' : ''}}>Pending</option>
                                                <option value="2" {{$getRequestDetails->status == 2 ? 'disabled' : ''}}>Approve</option>
                                                <option value="3" {{$getRequestDetails->status == 3 ? 'disabled' : ''}}>Rejected</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description / Reason :</label>
                                            <textarea rows="5" class="form-control square" name="leave_reason" readonly>{{$getRequestDetails->reason}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input class="btn btn-primary text-center" type="submit" value="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection