@extends('employee.template.layout')

@section('title', 'Leave Balance')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Leave Balance</h3>
        </div>
        <div class="content-header-left col-md-10 col-12 mb-2">
        </div>
    </div>

    <div class="content-body">
        <div class="row match-height">
            @foreach($allLeaves as $leave)
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <a href="javascript:void(0);">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="align-self-top">
                                            <i class="la la-calendar danger font-large-4"></i>
                                            <span class="d-block mb-1 font-medium-1">{{$leave->leave_type}}</span>
                                        </div>
                                        <div class="media-body text-center align-self-bottom mt-3">
                                            <h1 class="info mb-0">{{$leave->total_leave}}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer border-top-lighten-5 text-muted" style="margin: 20px;">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <span class="badge badge-primary badge-pill float-right">{{$leave->total_leave}}</span>
                                            Leave Total
                                        </li>
                                        <li class="list-group-item">
                                            <span class="badge badge-danger badge-pill float-right">{{isset($empTakenLeave) ? $empTakenLeave[$leave->leave_type] : 0}}</span>
                                            Leave Usage
                                        </li>
                                        <li class="list-group-item">
                                            <span class="badge badge-success badge-pill float-right">{{isset($existLeave) ? $existLeave[$leave->leave_type] : 0}}</span>
                                            Leave Available
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection