@extends('employee.template.layout')

@section('title', 'Leave Apply')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Leave Apply</h3>
        </div>
    </div>
    <div class="content-body">
        <form role="form" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">

                <div class="col-md-6 offset-3">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title text-center" id="basic-layout-square-controls">Apply For Leave Request</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                        </div>
                        <hr>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="form-group">
                                            <h4><b>Username (Employee Id)</b></h4>
                                            <b>{{ $emp->name.' ('.$emp->emp_id.')' }}</b>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Name :</label>
                                            <input type="text" class="form-control square" name="emp_name" value="{{ $emp->name }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group">
                                            <label>Select Leave Type :</label>
                                            <select name="leave_type_id" class="form-control square">
                                                <option value="">Select Leave type</option>
                                                @foreach($leaveTypes as $key => $type)
                                                <option value="{{$type->id}}" {{$type->leave_type == $key ? 'selected' : ''}}>{{$type->leave_type}}</option>
                                                @endforeach
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset class="form-group">
                                            <label>Select Leave mode :</label>
                                            <select name="leave_apply_type" class="form-control square">
                                                <option value="">Select Leave type</option>
                                                <option value="1">Full Day</option>
                                                <option value="2">half Day</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Assign Your TL / Manager :</label>
                                            <?php $TLDetails = \App\Models\Department::LeadNameByDept($emp->department_id) ?>
                                            <select name="leave_assign_to" class="form-control square">
                                                <option value="">Select Lead</option>
                                                @foreach($TLDetails as $details)
                                                <option value="{{$details->id}}">{{$details->name}}/( {{\App\Models\Employee::deptPosition($details->emp_position)}} )</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Time duration For leave :</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                                <input type="text" name="Leave_duration" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Time duration for Leave Apply" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description / Reason :</label>
                                            <textarea rows="5" class="form-control square" name="leave_reason"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input class="btn btn-primary text-center" type="submit" value="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection