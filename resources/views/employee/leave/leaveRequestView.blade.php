@extends('employee.template.layout')

@section('title', 'Leave View')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Leave View</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row offset-4">
            @if (session('errors'))
                <div class="alert alert-danger">
                    @foreach (session('errors')->all() as $error)
                        <span class="text-center">{{ $error }}</span>
                    @endforeach
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-8 offset-2">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-square-controls">Leave Information</h4>
                        <a class="heading-elements-toggle">
                            <i class="la la-ellipsis-v font-medium-3"></i>
                        </a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-xl-12">
                                    @if($getRequestDetails->count() == 0)
                                        <section id="collapsible">
                                            <div class="card">
                                                <div id="collapse1" class="card-collapse">
                                                    <div class="card mb-0">
                                                        <div class="card-header" id="headingAOne">
                                                            <h5 class="mb-0 text-center">
                                                                <button class="btn btn-link text-center" data-toggle="collapse" data-target="#collapseAOne" aria-expanded="true" aria-controls="collapseAOne">
                                                                    No Records Found here .....!!
                                                                </button>
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    @endif
                                    @foreach($getRequestDetails as $key => $getRequestData)
                                        <section id="collapsible">
                                            <div class="card">
                                                <div id="collapse1" class="card-collapse">
                                                    <div class="card mb-0">
                                                        <div class="card-header" id="headingA{{$key}}">
                                                            <h5 class="mb-0">
                                                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseA{{$key}}" aria-expanded="true" aria-controls="collapseA{{$key}}">
                                                                    @if(isset($getRequestData->start_date) and isset($getRequestData->end_date))
                                                                        Apply For <span class="badge-danger badge">{{\Carbon\Carbon::parse($getRequestData->start_date)->format('Y-m-d')}} - {{\Carbon\Carbon::parse($getRequestData->end_date)->format('Y-m-d')}}</span>  Duration
                                                                    @endif
                                                                </button>
                                                                <a style="float: right" href="{{route('emp-leave-request-approve',['id' => $getRequestData->id])}}">Edit</a>
                                                            </h5>
                                                        </div>
                                                        <div id="collapseA{{$key}}" class="collapse" aria-labelledby="headingA{{$key}}" style="">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-xl-6">
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item">

                                                                                <span class="badge badge-warning float-right">{{$getRequestData->leaveType->leave_type}}</span>
                                                                                Leave Type :
                                                                            </li>
                                                                            <li class="list-group-item">
                                                                                <span class="badge badge-info float-right">{{\Carbon\Carbon::parse($getRequestData->start_date)->format('Y-m-d')}} - {{\Carbon\Carbon::parse($getRequestData->end_date)->format('Y-m-d')}}</span>
                                                                                Time Duration :
                                                                            </li>
                                                                            <li class="list-group-item">
                                                                                @if(isset($getRequestData->approved_at))
                                                                                    <span class="badge badge-danger float-right">{{\Carbon\Carbon::parse($getRequestData->approved_at)->format('Y-m-d')}}</span>
                                                                                @else
                                                                                    <span class="badge badge-danger float-right">- -</span>
                                                                                @endif

                                                                                Approved On :
                                                                            </li>
                                                                        </ul>
                                                                    </div>

                                                                    <div class="col-lg-6 col-xl-6">
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item">
                                                                                <span class="badge badge-info float-right">{{\App\Models\EmpLeaveMaster::leaveApplyModes($getRequestData->leave_apply_type)}}</span>
                                                                                Mode of Leave :
                                                                            </li>
                                                                            <li class="list-group-item">
                                                                                <span class="badge badge-info float-right">
                                                                                {{$getRequestData->total_leave ? $getRequestData->total_leave : '- -'}}</span>
                                                                                No Of Day :
                                                                            </li>
                                                                            <li class="list-group-item">
                                                                                @if($getRequestData->status == 1)
                                                                                    <span class="badge badge-danger float-right">Pending</span>
                                                                                @elseif($getRequestData->status == 2)
                                                                                    <span class="badge badge-danger float-right">Approved</span>
                                                                                @else
                                                                                    <span class="badge badge-danger float-right">Reject</span>
                                                                                @endif
                                                                                Status :
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item">
                                                                                <span class="float-right">{{$getRequestData->reason ? $getRequestData->reason : '- -' }}</span>
                                                                                Description/Reason :
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection